import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            conditionIsShow : true
        };
        this.clickOnShow=this.clickOnShow.bind(this);
    }

    clickOnShow(){
        this.setState({
            conditionIsShow : !this.state.conditionIsShow
        });
    }

    clickOnRefresh(){
        window.location.reload();
    }

    ListOperation(){
        if(!this.state.conditionIsShow)
            return <TodoList/>;
        else
            return null;
    }

    render(){
        return (
            <div className='App'>
                <button onClick={this.clickOnShow}> {this.state.conditionIsShow?'Show':'Hide'}</button>
                <button onClick={this.clickOnRefresh}>Refresh</button>
                {this.ListOperation()}
            </div>
        );
    }

}

export default App;