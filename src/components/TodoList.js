import React, {Component} from 'react';
import './todolist.less';

class TodoList extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            index : 1,
            title : []
        };
        this.addList = this.addList.bind(this);
        console.log("constructor");
    }

    componentDidMount(){
        console.log("componentDidMount");
    }

    componentDidUpdate(){
        console.log("componentDidUpdate");
    }

    componentWillMount() {
        console.log("componentWillMount");
    }

    addList(){
        this.setState({
            index : this.state.index+1,
            title : [...this.state.title, <p key={this.state.index}>List Tittle {this.state.index}</p>]
        });
    }

    render() {
    return (
      <div className='TodoList'>
        <button onClick={this.addList}>Add</button>
          {this.state.title}
      </div>
    );
  }
}

export default TodoList;

